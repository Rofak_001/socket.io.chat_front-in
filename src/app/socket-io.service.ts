import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class SocketIoService {
  socket;
  private userSource = new Subject();
  listUser = this.userSource.asObservable();
  private idSource = new Subject();
  currentId = this.idSource.asObservable();

  constructor(private http: HttpClient, private cookie: CookieService) {
  }

  connect(myUsername): void {
    // @ts-ignore
    this.socket = io('http://127.0.0.1:8888/chat');
    this.socket.on('connect', () => {
      // register user
      if (this.cookie.check('user') === false) {
        this.registerUer(myUsername, this.socket.id).subscribe(res => {
          this.idSource.next(res);
          this.cookie.set('user', JSON.stringify(res));
          this.socket.emit('userJoin', {
            sessionId: this.socket.id,
            username: myUsername
          });
        });
      }
      // after register send event to socket to get user every time user register
      this.socket.emit('userJoin', {
        sessionId: this.socket.id,
        username: myUsername
      });
    });
  }

  // tslint:disable-next-line:variable-name
  sendMessage(sender_Id, sender_Name, receiver_Id, text, receiver_Name, file): void {
    // tslint:disable-next-line:prefer-const
    this.socket.emit('sendMessage', {
      message: text,
      senderId: sender_Id,
      senderName: sender_Name,
      receiverName: receiver_Name,
      receiverId: receiver_Id,
      dataImage: file,
    })
    ;
  }

  getMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('newMessage', (message) => {
        observer.next(message);
        console.log(message);
      });
    });
  }
  getUser = () => {
    return Observable.create((observer) => {
      this.socket.on('newUser', (data) => {
        observer.next(data);
        this.getAllUser().subscribe((res) => {
          this.userSource.next(res);
          console.log(res);
        });
      });
    });
  }

  getAllUser(): Observable<any> {
    return this.http.get('http://localhost:8080/users');
  }

  onUserTyping = () => {
    return Observable.create((observer) => {
      this.socket.on('userTyping', (data) => {
        observer.next(data);
      });
    });
  }

  // tslint:disable-next-line:variable-name
  userTyping(senderName, receiver_Name): void {
    this.socket.emit('userTyping', {
      senderId: this.socket.id,
      sender: senderName,
      receiverName: receiver_Name
    });
  }

  onUserStopTyping = () => {
    return Observable.create((observer) => {
      this.socket.on('userStopTyping', (data) => {
        observer.next(data);
      });
    });
  }

  // tslint:disable-next-line:variable-name
  userStopTyping(senderName, receiver_Name): void {
    this.socket.emit('userStopTyping', {
      senderId: this.socket.id,
      sender: senderName,
      receiverName: receiver_Name
    });
  }

  registerUer(currentUser, CurrentsessionId): Observable<any> {
    return this.http.post('http://localhost:8080/users', {
      sessionId: CurrentsessionId,
      username: currentUser
    });
  }

  getMessageByRoom(senderNameId, receiverNameId): Observable<any> {
    return this.http.get('http://localhost:8080/messages?senderNameId=' + senderNameId + '&receiverNameId=' + receiverNameId);
  }

  uploadImage(image: File): Observable<any> {
    const formData = new FormData();
    formData.append('file', image);
    return this.http.post('http://localhost:8080/upload', formData);
  }

}
