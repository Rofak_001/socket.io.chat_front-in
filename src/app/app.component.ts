import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {SocketIoService} from './socket-io.service';
import * as $ from 'jquery';
// @ts-ignore
import typingGif from '../assets/img/typing.gif';
import {CookieService} from 'ngx-cookie-service';
import {NgxImageCompressService} from 'ngx-image-compress';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'socket-io-chat';
  text;
  id;
  message;
  receiverSessionId;
  receiverId;
  chatWithUser;
  listUser;
  currentUser;
  currentUserId;
  typingTimer;
  receiverName;
  imgResultBeforeCompress;
  imgResultAfterCompress;
  image;
  selectedFile;
  @ViewChild('inbox_chat') inboxChat: ElementRef;
  @ViewChild('msg_history') msgHistory: ElementRef;

  constructor(private socketIoService: SocketIoService, private renderer2: Renderer2, private cookie: CookieService, private imageCompress: NgxImageCompressService) {
  }

  ngOnInit(): void {
    if (this.cookie.check('user')) {
      const user = JSON.parse(this.cookie.get('user'));
      this.currentUser = user.username;
      this.currentUserId = user.id;
      this.socketIoService.connect(this.currentUser);
    } else {
      this.currentUser = prompt('Enter Your name', '');
      this.socketIoService.connect(this.currentUser);
    }
    this.socketIoService.getMessages().subscribe(data => {
      this.render(data.message);
      this.image = data.image;
    });
    this.socketIoService.getUser().subscribe(data => {
      // this.renderUser(data.username, data.sessionId);
    });
    this.socketIoService.listUser.subscribe(res => {
      this.listUser = res;
    });
    this.socketIoService.onUserTyping().subscribe(data => {
      console.log(data);
      this.createTypingTemplate();
    });
    this.socketIoService.onUserStopTyping().subscribe(data => {
      console.log('stop');
      $('#typing').remove();
    });
    this.socketIoService.currentId.subscribe(res => {
      // @ts-ignore
      this.currentUserId = res.id;
    });
  }

  sendMessage(): void {
    console.log(this.currentUserId);
    this.socketIoService.sendMessage(this.currentUserId, this.currentUser, this.receiverId, this.text, this.receiverName, this.imgResultAfterCompress);
    this.displaySenderMessage(this.text);
    // const divOutgoing = this.renderer2.createElement('div');
    // divOutgoing.classList.add('outgoing_msg');
    // const divSentMsg = this.renderer2.createElement('div');
    // divSentMsg.classList.add('sent_msg');
    // const p = this.renderer2.createElement('p');
    // const text = this.renderer2.createText(this.text);
    // p.append(text);
    // divSentMsg.append(p);
    // divOutgoing.append(divSentMsg);
    // this.renderer2.appendChild(this.msgHistory.nativeElement, divOutgoing);
  }

  renderUser(name, sessionId): void {
    const chatList = this.renderer2.createElement('div');
    chatList.classList.add('chat_list');
    chatList.addEventListener('click', () => {
      this.receiverSessionId = sessionId;
      this.chatWithUser = name;
    });
    const chatPeople = this.renderer2.createElement('div');
    chatPeople.classList.add('chat_people');
    const chatImg = this.renderer2.createElement('div');
    chatImg.classList.add('chat_img');
    const img = this.renderer2.createElement('img');
    img.src = 'https://bootdey.com/img/Content/avatar/avatar2.png';
    img.alt = 'sunil';
    const chatIb = this.renderer2.createElement('div');
    chatIb.classList.add('chat_ib');
    const h5 = this.renderer2.createElement('h5');
    const text = this.renderer2.createText(name);
    chatList.append(chatPeople);
    chatPeople.append(chatImg);
    chatPeople.append(chatIb);
    chatImg.append(img);
    chatIb.append(h5);
    h5.append(text);
    this.renderer2.appendChild(this.inboxChat.nativeElement, chatList);
  }

  render(message): void {
    // tslint:disable-next-line:variable-name
    const incoming_msg = this.renderer2.createElement('div');
    incoming_msg.classList.add('incoming_msg');
    // tslint:disable-next-line:variable-name
    const received_msg = this.renderer2.createElement('div');
    received_msg.classList.add('received_msg"');
    // tslint:disable-next-line:variable-name
    const received_withd_msg = this.renderer2.createElement('div');
    received_withd_msg.classList.add('received_withd_msg');
    const p = this.renderer2.createElement('p');
    const text = this.renderer2.createText(message);
    p.append(text);
    received_withd_msg.append(p);
    received_msg.append(received_withd_msg);
    incoming_msg.append(received_msg);
    this.renderer2.appendChild(this.msgHistory.nativeElement, incoming_msg);
  }

  selectUser(receiverName, sessionId, receiverId): void {
    $('.outgoing_msg').remove();
    $('.incoming_msg').remove();
    this.receiverSessionId = sessionId;
    this.receiverName = receiverName;
    this.receiverId = receiverId;
    console.log('Current UserID:' + this.currentUserId);
    console.log('receiverSessionId:' + sessionId);
    console.log('receiverId:' + receiverId);
    console.log('recieverName:' + this.receiverName);
    const senderNameId = this.currentUserId + this.currentUser + receiverId + receiverName;
    const receiverNameId = receiverId + receiverName + this.currentUserId + this.currentUser;
    console.log('Name:' + senderNameId);
    console.log('Name:' + receiverNameId);
    this.socketIoService.getMessageByRoom(senderNameId, receiverNameId).subscribe(res => {
      res.map(r => {
        console.log(res)
        if (r.receiverName === this.currentUser) {
          this.render(r.message);
          this.image = 'data:image/png;base64,' + r.dataImage;
        } else {
          this.displaySenderMessage(r.message);
        }
      });
    });
  }

  userTyping(): void {
    this.socketIoService.userTyping(this.currentUser, this.receiverName);
    clearTimeout(this.typingTimer);
    this.typingTimer = setTimeout(() => {
      this.socketIoService.userStopTyping(this.currentUser, this.receiverName);
    }, 1000);
  }

  createTypingTemplate(): void {
    if ($('#typing').length === 0) {
      // tslint:disable-next-line:variable-name
      const incoming_msg = this.renderer2.createElement('div');
      incoming_msg.classList.add('incoming_msg');
      // incoming_msg.classList.add('type');
      incoming_msg.setAttribute('id', 'typing');
      // tslint:disable-next-line:variable-name
      const received_msg = this.renderer2.createElement('div');
      received_msg.classList.add('received_msg"');
      const img = this.renderer2.createElement('img');
      img.src = typingGif;
      img.classList.add('typing');
      incoming_msg.append(received_msg);
      received_msg.append(img);
      this.renderer2.appendChild(this.msgHistory.nativeElement, incoming_msg);
    }
  }

  displaySenderMessage(message): void {
    const divOutgoing = this.renderer2.createElement('div');
    divOutgoing.classList.add('outgoing_msg');
    const divSentMsg = this.renderer2.createElement('div');
    divSentMsg.classList.add('sent_msg');
    const p = this.renderer2.createElement('p');
    const text = this.renderer2.createText(message);
    p.append(text);
    divSentMsg.append(p);
    divOutgoing.append(divSentMsg);
    this.renderer2.appendChild(this.msgHistory.nativeElement, divOutgoing);
  }

  compressFile(): void {
    this.imageCompress.uploadFile().then(({image, orientation}) => {
      this.imgResultBeforeCompress = image;
      console.warn('Size in bytes was:', this.imageCompress.byteCount(image));

      this.imageCompress.compressFile(image, orientation, 50, 50).then(
        result => {
          this.imgResultAfterCompress = result;
          console.warn('Size in bytes is now:', this.imageCompress.byteCount(result));
        }
      );

    });
  }
}

